package com.egnyte.utils.auditreporter.service;

import com.csvreader.CsvReader;
import com.egnyte.utils.auditreporter.model.User;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static java.util.UUID.fromString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ResourceParserTest {

    @Mock
    private CsvReader usersReader;
    @Mock
    private CsvReader filesReader;
    @InjectMocks
    private ResourceParser parser;

    @BeforeMethod
    public void setUp() throws FileNotFoundException {
        parser = new ResourceParser();
        initMocks(this);
    }

    @Test(
            description = "Should get users from resource CSV files"
    )
    public void getUsers() throws IOException {
        when(usersReader.readRecord()).thenReturn(true, true, false);
        when(usersReader.get("USER_ID")).thenReturn("1", "2");
        when(usersReader.get("USER_NAME")).thenReturn("user1", "user2");
        when(filesReader.readRecord()).thenReturn(true, true, true, false);
        when(filesReader.get("FILE_ID")).thenReturn("5974448e-9afd-4c9a-ac5a-9b1e84227820", "fab16fa4-8251-4394-a673-c961a65eb1d2", "b4f3eecf-95aa-42a7-bffc-83a5441b7d2e");
        when(filesReader.get("SIZE")).thenReturn("100", "200", "300");
        when(filesReader.get("FILE_NAME")).thenReturn("picture.jpg", "movie.avi", "notes.txt");
        when(filesReader.get("OWNER_USER_ID")).thenReturn("1", "1", "2");

        List<User> actualUsers = parser.getUsers();

        verify(usersReader).readHeaders();
        verify(filesReader).readHeaders();

        assertThat(actualUsers).hasSize(2);

        User firstUser = actualUsers.get(0);
        assertThat(firstUser).isNotNull();
        assertThat(firstUser.getId()).isEqualTo(1);
        assertThat(firstUser.getName()).isEqualTo("user1");
        assertThat(firstUser.getFiles()).hasSize(2);
        assertThat(firstUser.getFiles().get(0).getId()).isEqualTo(fromString("5974448e-9afd-4c9a-ac5a-9b1e84227820"));
        assertThat(firstUser.getFiles().get(0).getName()).isEqualTo("picture.jpg");
        assertThat(firstUser.getFiles().get(0).getSize()).isEqualTo(100);
        assertThat(firstUser.getFiles().get(0).getOwner()).isNotNull();
        assertThat(firstUser.getFiles().get(0).getOwner().getId()).isEqualTo(1);
        assertThat(firstUser.getFiles().get(1).getId()).isEqualTo(fromString("fab16fa4-8251-4394-a673-c961a65eb1d2"));
        assertThat(firstUser.getFiles().get(1).getName()).isEqualTo("movie.avi");
        assertThat(firstUser.getFiles().get(1).getSize()).isEqualTo(200);
        assertThat(firstUser.getFiles().get(1).getOwner()).isNotNull();
        assertThat(firstUser.getFiles().get(1).getOwner().getId()).isEqualTo(1);

        User secondUser = actualUsers.get(1);
        assertThat(secondUser).isNotNull();
        assertThat(secondUser.getId()).isEqualTo(2);
        assertThat(secondUser.getName()).isEqualTo("user2");
        assertThat(secondUser.getFiles()).hasSize(1);
        assertThat(secondUser.getFiles().get(0).getId()).isEqualTo(fromString("b4f3eecf-95aa-42a7-bffc-83a5441b7d2e"));
        assertThat(secondUser.getFiles().get(0).getName()).isEqualTo("notes.txt");
        assertThat(secondUser.getFiles().get(0).getSize()).isEqualTo(300);
        assertThat(secondUser.getFiles().get(0).getOwner()).isNotNull();
        assertThat(secondUser.getFiles().get(0).getOwner().getId()).isEqualTo(2);
    }
}
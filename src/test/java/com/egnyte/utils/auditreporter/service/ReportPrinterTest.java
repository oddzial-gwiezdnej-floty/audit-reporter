package com.egnyte.utils.auditreporter.service;

import com.egnyte.utils.auditreporter.model.FileInfo;
import com.egnyte.utils.auditreporter.model.User;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static java.lang.System.setOut;
import static java.util.Arrays.asList;
import static java.util.UUID.fromString;
import static org.assertj.core.api.Assertions.assertThat;

public class ReportPrinterTest {

    private OutputStream out;

    private ReportPrinter printer;

    @BeforeMethod
    public void setUp() {
        setOut(new PrintStream(out = new ByteArrayOutputStream()));
        printer = new ReportPrinter();
    }

    @Test(
            description = "Should print report in normal format"
    )
    public void printNormalOutput() {
        User user1 = new User(1, "user1");
        FileInfo file1 = new FileInfo(fromString("5974448e-9afd-4c9a-ac5a-9b1e84227820"), 100, "picture.jpg", user1);
        user1.addFile(file1);
        User user2 = new User(2, "user2");
        FileInfo file2 = new FileInfo(fromString("fab16fa4-8251-4394-a673-c961a65eb1d2"), 200, "movie.avi", user2);
        user2.addFile(file2);
        printer.addUsers(asList(user1, user2));

        printer.print();

        assertThat(out.toString()).isEqualTo("" +
                        "Audit Report\r\n" +
                        "============\r\n" +
                        "## User: user1\r\n" +
                        "* picture.jpg ==> 100 bytes\r\n" +
                        "## User: user2\r\n" +
                        "* movie.avi ==> 200 bytes\r\n"
        );
    }

    @Test(
            description = "Should print report in CSV format"
    )
    public void printCSVOutput() {
        User user1 = new User(1, "user1");
        FileInfo file1 = new FileInfo(fromString("5974448e-9afd-4c9a-ac5a-9b1e84227820"), 100, "picture.jpg", user1);
        user1.addFile(file1);
        User user2 = new User(2, "user2");
        FileInfo file2 = new FileInfo(fromString("fab16fa4-8251-4394-a673-c961a65eb1d2"), 200, "movie.avi", user2);
        user2.addFile(file2);
        printer.addUsers(asList(user1, user2)).addCSVFormat(true);

        printer.print();

        assertThat(out.toString()).isEqualTo("" +
                        "user1,picture.jpg,100\r\n" +
                        "user2,movie.avi,200\r\n"
        );
    }

    @Test(
            description = "Should print report in top format for the biggest file"
    )
    public void printTopOutput() {
        User user1 = new User(1, "user1");
        FileInfo file1 = new FileInfo(fromString("5974448e-9afd-4c9a-ac5a-9b1e84227820"), 100, "picture.jpg", user1);
        user1.addFile(file1);
        User user2 = new User(2, "user2");
        FileInfo file2 = new FileInfo(fromString("fab16fa4-8251-4394-a673-c961a65eb1d2"), 200, "movie.avi", user2);
        user2.addFile(file2);
        printer.addUsers(asList(user1, user2)).addTopFormat(1);

        printer.print();

        assertThat(out.toString()).isEqualTo("" +
                        "Top #1 Report\r\n" +
                        "=============\r\n" +
                        "* movie.avi ==> user user2, 200 bytes\r\n"
        );
    }

    @Test(
            description = "Should print report in top format for all files"
    )
    public void printTopOutputForAllFiles() {
        User user1 = new User(1, "user1");
        FileInfo file1 = new FileInfo(fromString("5974448e-9afd-4c9a-ac5a-9b1e84227820"), 100, "picture.jpg", user1);
        user1.addFile(file1);
        User user2 = new User(2, "user2");
        FileInfo file2 = new FileInfo(fromString("fab16fa4-8251-4394-a673-c961a65eb1d2"), 200, "movie.avi", user2);
        user2.addFile(file2);
        printer.addUsers(asList(user1, user2)).addTopFormat(10);

        printer.print();

        assertThat(out.toString()).isEqualTo("" +
                        "Top #10 Report\r\n" +
                        "=============\r\n" +
                        "* movie.avi ==> user user2, 200 bytes\r\n" +
                        "* picture.jpg ==> user user1, 100 bytes\r\n"
        );
    }

    @Test(
            description = "Should print report in top and CSV format for the biggest file"
    )
    public void printTopInCSVOutput() {
        User user1 = new User(1, "user1");
        FileInfo file1 = new FileInfo(fromString("5974448e-9afd-4c9a-ac5a-9b1e84227820"), 100, "picture.jpg", user1);
        user1.addFile(file1);
        User user2 = new User(2, "user2");
        FileInfo file2 = new FileInfo(fromString("fab16fa4-8251-4394-a673-c961a65eb1d2"), 200, "movie.avi", user2);
        user2.addFile(file2);
        printer.addUsers(asList(user1, user2)).addTopFormat(1).addCSVFormat(true);

        printer.print();

        assertThat(out.toString()).isEqualTo("" +
                        "movie.avi,user2,200\r\n"
        );
    }

    @Test(
            description = "Should print report in top and CSV format for all files"
    )
    public void printTopInCSVOutputForAllFiles() {
        User user1 = new User(1, "user1");
        FileInfo file1 = new FileInfo(fromString("5974448e-9afd-4c9a-ac5a-9b1e84227820"), 100, "picture.jpg", user1);
        user1.addFile(file1);
        User user2 = new User(2, "user2");
        FileInfo file2 = new FileInfo(fromString("fab16fa4-8251-4394-a673-c961a65eb1d2"), 200, "movie.avi", user2);
        user2.addFile(file2);
        printer.addUsers(asList(user1, user2)).addTopFormat(-12).addCSVFormat(true);

        printer.print();

        assertThat(out.toString()).isEqualTo("" +
                        "movie.avi,user2,200\r\n" +
                        "picture.jpg,user1,100\r\n"
        );
    }

    @AfterMethod
    public void tearDown() {
        setOut(null);
    }
}
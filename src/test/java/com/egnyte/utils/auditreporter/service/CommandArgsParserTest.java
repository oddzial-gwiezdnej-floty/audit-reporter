package com.egnyte.utils.auditreporter.service;

import org.apache.commons.cli.ParseException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CommandArgsParserTest {

    @DataProvider(name = "Users file command args")
    public static Object[][] usersFileArgsProvider() {
        return new Object[][]{
                {new String[]{"--users", "path/to/file"}, "path/to/file"},
                {new String[]{"-users", "path/to/file", "--files", "some/other/path"}, "path/to/file"},
                {new String[]{"-files", "path/to/file"}, null},
                {new String[]{"-users", "path/to/file"}, "path/to/file"},
                {new String[]{"-c"}, null},
                {null, null}
        };
    }

    @Test(
            dataProvider = "Users file command args",
            description = "Should get proper path to users file from command args"
    )
    public void getProperUsersFilePath(String[] args, String pathToFile) throws ParseException {
        CommandArgsParser parser = new CommandArgsParser(args);

        assertThat(parser.getUserFilePath()).isEqualTo(pathToFile);
    }

    @DataProvider(name = "Files file command args")
    public static Object[][] filesFileArgsProvider() {
        return new Object[][]{
                {new String[]{"--files", "path/to/file"}, "path/to/file"},
                {new String[]{"-files", "path/to/file", "--users", "some/other/path"}, "path/to/file"},
                {new String[]{"-users", "path/to/file"}, null},
                {new String[]{"-files", "path/to/file"}, "path/to/file"},
                {new String[]{"-c"}, null},
                {null, null}
        };
    }

    @Test(
            dataProvider = "Files file command args",
            description = "Should get proper path to files file from command args"
    )
    public void getProperFilesFilePath(String[] args, String pathToFile) throws ParseException {
        CommandArgsParser parser = new CommandArgsParser(args);

        assertThat(parser.getFilesFilePath()).isEqualTo(pathToFile);
    }

    @DataProvider(name = "CSV output command args")
    public static Object[][] csvOutputArgsProvider() {
        return new Object[][]{
                {new String[]{"-c"}, true},
                {new String[]{"-c", "--users", "some/other/path"}, true},
                {new String[]{"-users", "path/to/file"}, false},
                {null, false}
        };
    }

    @Test(
            dataProvider = "CSV output command args",
            description = "Should get information about CSV formatted output from command args"
    )
    public void getInfoAboutCSVOutput(String[] args, boolean csvOutput) throws ParseException {
        CommandArgsParser parser = new CommandArgsParser(args);

        assertThat(parser.isCSVOutputRequested()).isEqualTo(csvOutput);
    }

    @DataProvider(name = "Top number of files command args")
    public static Object[][] topFilesArgsProvider() {
        return new Object[][]{
                {new String[]{"--top", "3"}, 3},
                {new String[]{"--top", "3", "--users", "some/other/path"}, 3},
                {new String[]{"-top", "3"}, 3},
                {new String[]{"-files", "path/to/file"}, null},
                {new String[]{"-c"}, null},
                {null, null}
        };
    }

    @Test(
            dataProvider = "Top number of files command args",
            description = "Should get proper number of top files from command args"
    )
    public void getProperFilesFilePath(String[] args, Integer numberOfFiles) throws ParseException {
        CommandArgsParser parser = new CommandArgsParser(args);

        assertThat(parser.getNumberOfFilesToDisplay()).isEqualTo(numberOfFiles);
    }
}
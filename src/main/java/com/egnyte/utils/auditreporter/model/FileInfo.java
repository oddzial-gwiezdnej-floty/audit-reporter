package com.egnyte.utils.auditreporter.model;

import java.util.UUID;

public class FileInfo implements Comparable<FileInfo> {

    private UUID id;
    private int size;
    private String name;
    private User owner;

    public FileInfo(UUID id, int size, String name, User owner) {
        this.id = id;
        this.size = size;
        this.name = name;
        this.owner = owner;
    }

    public UUID getId() {
        return id;
    }

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public User getOwner() {
        return owner;
    }

    @Override
    public int compareTo(FileInfo file) {
        if (file == null) {
            return -1;
        }
        return file.getSize() - size;
    }
}

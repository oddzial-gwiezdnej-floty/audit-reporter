package com.egnyte.utils.auditreporter.model;

import java.util.ArrayList;
import java.util.List;

public class User {

    private int id;
    private String name;
    private List<FileInfo> files = new ArrayList<>();

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<FileInfo> getFiles() {
        return files;
    }

    public void addFile(FileInfo file) {
        files.add(file);
    }
}

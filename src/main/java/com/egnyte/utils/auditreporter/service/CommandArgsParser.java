package com.egnyte.utils.auditreporter.service;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CommandArgsParser {

    private static final String USERS_FILE_ARG = "users";
    private static final String FILES_FILE_ARG = "files";
    private static final String CSV_OUTPUT_ARG = "c";
    private static final String TOP_OUTPUT_ARG = "top";

    private CommandLine commandLine;
    private Options options = new Options();

    public CommandArgsParser(String[] args) throws ParseException {
        setAvailableOptions();
        setCommandLine(args);
    }

    public String getUserFilePath() {
        return commandLine.getOptionValue(USERS_FILE_ARG);
    }

    public String getFilesFilePath() {
        return commandLine.getOptionValue(FILES_FILE_ARG);
    }

    public boolean isCSVOutputRequested() {
        return commandLine.hasOption(CSV_OUTPUT_ARG);
    }

    public Integer getNumberOfFilesToDisplay() {
        if (commandLine.hasOption(TOP_OUTPUT_ARG)) {
            return Integer.valueOf(commandLine.getOptionValue(TOP_OUTPUT_ARG));
        }
        return null;
    }

    private void setAvailableOptions() {
        options.addOption(null, USERS_FILE_ARG, true, "Sets a path to file which contains users data");
        options.addOption(null, FILES_FILE_ARG, true, "Sets a path to file which contains files data");
        options.addOption(CSV_OUTPUT_ARG, false, "Print report in CSV format");
        options.addOption(null, TOP_OUTPUT_ARG, true, "Print n largest files sorted by size");
    }

    private void setCommandLine(String[] args) throws ParseException {
        commandLine = new BasicParser().parse(options, args);
    }
}

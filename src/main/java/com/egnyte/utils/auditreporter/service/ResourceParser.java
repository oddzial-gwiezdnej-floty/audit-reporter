package com.egnyte.utils.auditreporter.service;

import com.csvreader.CsvReader;
import com.egnyte.utils.auditreporter.model.FileInfo;
import com.egnyte.utils.auditreporter.model.User;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.Integer.valueOf;
import static java.util.UUID.fromString;

public class ResourceParser {

    private static final String USER_ID_HEADER = "USER_ID";
    private static final String USER_NAME_HEADER = "USER_NAME";
    private static final String FILE_ID_HEADER = "FILE_ID";
    private static final String FILE_SIZE_HEADER = "SIZE";
    private static final String FILE_NAME_HEADER = "FILE_NAME";
    private static final String FILE_OWNER_HEADER = "OWNER_USER_ID";

    private CsvReader usersReader;
    private CsvReader filesReader;

    public ResourceParser(String usersFilePath, String filesFilePath) throws FileNotFoundException {
        usersReader = new CsvReader(usersFilePath);
        filesReader = new CsvReader(filesFilePath);
    }

    public List<User> getUsers() throws IOException {
        List<User> users = new ArrayList<>();
        try {
            readUsers(users);
            readFiles(users);
        } finally {
            closeReaders();
        }
        return users;
    }

    private void readUsers(List<User> users) throws IOException {
        usersReader.readHeaders();
        while (usersReader.readRecord()) {
            int userId = valueOf(usersReader.get(USER_ID_HEADER));
            String userName = usersReader.get(USER_NAME_HEADER);
            users.add(new User(userId, userName));
        }
    }

    private void readFiles(List<User> users) throws IOException {
        filesReader.readHeaders();
        while (filesReader.readRecord()) {
            UUID fileId = fromString(filesReader.get(FILE_ID_HEADER));
            String fileName = filesReader.get(FILE_NAME_HEADER);
            int fileSize = valueOf(filesReader.get(FILE_SIZE_HEADER));
            User fileOwner = getUser(users, valueOf(filesReader.get(FILE_OWNER_HEADER)));
            FileInfo file = new FileInfo(fileId, fileSize, fileName, fileOwner);
            fileOwner.addFile(file);
        }
    }

    private void closeReaders() {
        usersReader.close();
        filesReader.close();
    }

    private User getUser(List<User> users, int userId) {
        for (User user : users) {
            if (user.getId() == userId) {
                return user;
            }
        }
        return null;
    }

    ResourceParser() {
    }
}

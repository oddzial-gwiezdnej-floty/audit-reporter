package com.egnyte.utils.auditreporter.service;

import com.egnyte.utils.auditreporter.model.FileInfo;
import com.egnyte.utils.auditreporter.model.User;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.MAX_VALUE;
import static java.util.Collections.sort;

public class ReportPrinter {

    private List<User> users = new ArrayList<>();
    private boolean csvFormat;
    private Integer numberOfTopFiles;

    public ReportPrinter addUsers(List<User> users) {
        this.users.addAll(users);
        return this;
    }

    public ReportPrinter addCSVFormat(boolean csvFormat) {
        this.csvFormat = csvFormat;
        return this;
    }

    public ReportPrinter addTopFormat(Integer numberOfFiles) {
        numberOfTopFiles = numberOfFiles;
        if (numberOfTopFiles != null && numberOfTopFiles < 1) {
            numberOfTopFiles = MAX_VALUE;
        }
        return this;
    }

    public void print() {
        if (csvFormat && numberOfTopFiles != null) {
            printTopInCSV();
        } else if (numberOfTopFiles != null) {
            printTop();
        } else if (csvFormat) {
            printCSV();
        } else {
            printNormal();
        }
    }

    private void printNormal() {
        System.out.println("Audit Report");
        System.out.println("============");
        for (User user : users) {
            System.out.println("## User: " + user.getName());
            for (FileInfo file : user.getFiles()) {
                System.out.println("* " + file.getName() + " ==> " + file.getSize() + " bytes");
            }
        }
    }

    private void printCSV() {
        for (User user : users) {
            for (FileInfo file : user.getFiles()) {
                System.out.println(user.getName() + "," + file.getName() + "," + file.getSize());
            }
        }
    }

    private void printTop() {
        System.out.println("Top #" + numberOfTopFiles + " Report");
        System.out.println("=============");
        List<FileInfo> files = getSortedFiles();
        for (FileInfo file : files) {
            System.out.println("* " + file.getName() + " ==> " + "user " + file.getOwner().getName() + ", " + file.getSize() + " bytes");
        }
    }

    private void printTopInCSV() {
        List<FileInfo> files = getSortedFiles();
        for (FileInfo file : files) {
            System.out.println(file.getName() + "," + file.getOwner().getName() + "," + file.getSize());
        }
    }

    private List<FileInfo> getSortedFiles() {
        List<FileInfo> files = new ArrayList<>();
        for (User user : users) {
            for (FileInfo file : user.getFiles()) {
                files.add(file);
            }
        }
        sort(files);
        if (files.size() > numberOfTopFiles) {
            files = files.subList(0, numberOfTopFiles);
        }
        return files;
    }
}

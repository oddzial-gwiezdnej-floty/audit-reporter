package com.egnyte.utils.auditreporter;

import com.egnyte.utils.auditreporter.service.CommandArgsParser;
import com.egnyte.utils.auditreporter.service.ReportPrinter;
import com.egnyte.utils.auditreporter.service.ResourceParser;
import org.apache.commons.cli.ParseException;

import java.io.IOException;

public class Runner {

    public static void main(String[] args) throws IOException, ParseException {
        Runner runner = new Runner();
        runner.run(args);
    }

    public void run(String[] args) throws IOException, ParseException {
        CommandArgsParser argsParser = new CommandArgsParser(args);
        ResourceParser resourceParser = new ResourceParser(argsParser.getUserFilePath(), argsParser.getFilesFilePath());
        ReportPrinter reportPrinter = new ReportPrinter()
                .addUsers(resourceParser.getUsers())
                .addCSVFormat(argsParser.isCSVOutputRequested())
                .addTopFormat(argsParser.getNumberOfFilesToDisplay());
        reportPrinter.print();
    }
}
